crab_positions = File.read("input.txt").split(',').map(&:to_i)
max_pos = crab_positions.max
min_pos = crab_positions.min

def diff(a, b)
    (a - b).abs
end

fuel_used = diff(max_pos, min_pos) ** 3
best_position = -1

def fuel_cost(length)
    fuel_sum = 0
    for cost in 1..length
        fuel_sum += cost
    end
    fuel_sum
end

for position in min_pos..max_pos do
    fuel_used_per_crab = []
    crab_positions.each { |crab_position| fuel_used_per_crab.push(fuel_cost(diff(position, crab_position))) }
    total_fuel_cost = fuel_used_per_crab.reduce(:+)
    if total_fuel_cost < fuel_used
        fuel_used = total_fuel_cost
        best_position = position
    end
end

puts "The best position is: " + best_position.to_s
puts "Fuel used: " + fuel_used.to_s