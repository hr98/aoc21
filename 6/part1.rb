days = 80

lanter_fishy_timers = File.read("input.txt").split(',').map(&:to_i)

(1..days).each do |day|
    current_count = lanter_fishy_timers.size
    (0...current_count).each do |i|
        lanter_fishy_timers[i] -= 1
        if lanter_fishy_timers[i] < 0
            lanter_fishy_timers.push(8) 
            lanter_fishy_timers[i] = 6
        end
    end
end

puts "Total Fishies: " + lanter_fishy_timers.size.to_s