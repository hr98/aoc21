$days = 256

lanter_fishy_timers = File.read("input.txt").split(',').map(&:to_i)

regular_counter = Array.new(7){0}
new_born_counter = Array.new(4){0}

for i in 1..5
    regular_counter[i] = lanter_fishy_timers.count { |fish| fish == i }
end

for i in 0...$days
    new_born_counter[(i - 1) % 4] += regular_counter[i % 7]
    regular_counter[(i - 1) % 7] += new_born_counter[i % 4]
    new_born_counter[i % 4] = 0
end

puts "Total Fishies: " + (new_born_counter.reduce(:+) + regular_counter.reduce(:+)).to_s