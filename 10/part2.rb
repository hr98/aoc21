$AutoCompleteScores = {
    '(' => 1,
    '[' => 2,
    '{' => 3,
    '<' => 4
}

$MatchingBracket = {
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>'
}

$Input = File.open('input.txt').map do |line|
    line = line.split('')
    line.pop
    line
end

line_scores = []

$Input.each do |line|
    stack = []
    corrupt = false
    line_score = 0
    while !line.empty?
        symbol = line.shift
        if ['(', '[', '{', '<'].include?(symbol)
            stack.push(symbol)
        else
            corrupt = true unless symbol == $MatchingBracket[stack.pop]
        end
    end
    next if corrupt
    while !stack.empty?
        line_score *= 5
        line_score += $AutoCompleteScores[stack.pop]
    end
    line_scores.push(line_score)
end

puts line_scores.sort[line_scores.size / 2]