$IllegalScores = {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137
}

$MatchingBracket = {
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>'
}

$Input = File.open('input.txt').map do |line|
    line = line.split('')
    line.pop
    line
end

score = 0

$Input.each do |line|
    stack = []
    while !line.empty?
        symbol = line.shift
        if ['(', '[', '{', '<'].include?(symbol)
            stack.push(symbol)
        else
            score += $IllegalScores[symbol] unless symbol == $MatchingBracket[stack.pop]
        end
    end
end

puts score