x_size = 10
y_size = 10
steps = 1000

octopuses = File.open('input.txt').map do |line|
    line = line.split('')
    line.pop
    line.map(&:to_i)
end

total_flash_counter = 0
(1..steps).each do |step|
    flash_map = Array.new(10) { Array.new(10) {0} }
    flash_counter = 0
    octopuses.map! { |row| row.map! { |octopus| octopus += 1 } }
    loop do
        octopuses.each_with_index do |row, y|
            row.each_with_index do |octopus, x| 
                next unless flash_map[y][x] == 0
                if octopuses[y][x] > 9
                    flash_map[y][x] = 1
                    for i in -1..1
                        target_y = y + i
                        next if target_y < 0 || target_y >= y_size
                        for j in -1..1
                            target_x = x + j
                            next if target_x < 0 || target_x >= x_size
                            octopuses[target_y][target_x] += 1
                        end
                    end
                end
            end
        end
        new_flash_counter = flash_map.reduce(0) { |sum, row| sum += row.reduce(:+) }
        break if flash_counter == new_flash_counter
        flash_counter = new_flash_counter
    end
    puts "Big flash: " + step.to_s if flash_counter == 100
    total_flash_counter += flash_counter
    flash_map.each_with_index { |row, y| row.each_with_index { |flash, x| octopuses[y][x] = 0 if flash == 1 } }
end
puts "Total Flashes: " + total_flash_counter.to_s