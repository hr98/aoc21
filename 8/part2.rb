sum = 0

def bad_fix(n) 
    (0...3-n).reduce(1) { |x, n| x * 10 }
end

File.open('input.txt').each do |line|
    split_input = line.split('|')
    patern_digits = split_input[0].split(' ')
    output_digits = split_input[1].split(' ')

    digit_1 = patern_digits.find { |x| x.size == 2 }
    digit_4 = patern_digits.find { |x| x.size == 4 }
    digit_7 = patern_digits.find { |x| x.size == 3 }
    digit_8 = patern_digits.find { |x| x.size == 7 }

    digits_6_long = patern_digits.select { |x| x.size == 6 }
    digits_5_long = patern_digits.select { |x| x.size == 5 }

    digit_6 = digits_6_long.find { |x| x.split('').count { |char| digit_1.split('').include?(char) } == 1 }
    digit_3 = digits_5_long.find { |x| x.split('').count { |char| digit_1.split('').include?(char) } == 2 }
    
    digits_6_long.delete(digit_6)
    digit_9 = digits_6_long.find { |x| (x.split('') | digit_3.split('')).size == x.size}
    digit_0 = digits_6_long.find { |x| x != digit_9 }

    digits_5_long.delete(digit_3)
    digit_5 = digits_5_long.find { |x| (x.split('') | digit_6.split('')).size == x.size + 1 }
    digit_2 = digits_5_long.find { |x| x != digit_5 }

    output_digits.each_with_index do |digit, index|
        case digit.size
        when 2
            sum += 1 * bad_fix(index)
        when 4
            sum += 4 * bad_fix(index)
        when 3
            sum += 7 * bad_fix(index)
        when 7
            sum += 8 * bad_fix(index)
        when 5
            if (digit.split('') | digit_2.split('')).size == digit.size
                sum += 2 * bad_fix(index)
            elsif (digit.split('') | digit_3.split('')).size == digit.size
                sum += 3 * bad_fix(index)
            elsif (digit.split('') | digit_5.split('')).size == digit.size
                sum += 5 * bad_fix(index)
            end
        when 6
            if (digit.split('') | digit_6.split('')).size == digit.size
                sum += 6 * bad_fix(index)
            elsif (digit.split('') | digit_9.split('')).size == digit.size
                sum += 9 * bad_fix(index)         
            end
        end
    end
end

puts sum