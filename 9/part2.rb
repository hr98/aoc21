hightmap = []
low_points = []
basin_sizes = []

File.open('input.txt').each do |line|
    line = line.split('')
    line.pop(2)
    line.map!(&:to_i)
    hightmap << line
end

hightmap.each_index do |row_i|
    hightmap[row_i].each_index do |column_i|
        hight = hightmap[row_i][column_i]
        next if row_i > 0 && hight >= hightmap[row_i - 1][column_i]
        next if row_i < hightmap.size - 1 && hight >= hightmap[row_i + 1][column_i]
        next if column_i > 0 && hight >= hightmap[row_i][column_i - 1]
        next if column_i < hightmap[row_i].size - 1 && hight >= hightmap[row_i][column_i + 1]
        low_points << [row_i, column_i]
    end
end

low_points.each do |low_point|
    nodes = []
    next_nodes = [low_point]
    while !next_nodes.empty?
        current_node = next_nodes.pop
        nodes << current_node
        hight = hightmap[current_node[0]][current_node[1]]
        if current_node[0] > 0
            next_node = [current_node[0] - 1, current_node[1]]
            other_hight = hightmap[current_node[0] - 1][current_node[1]]
            next_nodes << next_node if other_hight < 9 && other_hight > hight && !nodes.include?(next_node) && !next_nodes.include?(next_node)
        end
        if current_node[0] < 99
            next_node = [current_node[0] + 1, current_node[1]]
            other_hight = hightmap[current_node[0] + 1][current_node[1]]
            next_nodes << next_node if other_hight < 9 && other_hight > hight && !nodes.include?(next_node) && !next_nodes.include?(next_node)
        end
        if current_node[1] > 0
            next_node = [current_node[0], current_node[1] - 1]
            other_hight = hightmap[current_node[0]][current_node[1] - 1]
            next_nodes << next_node if other_hight < 9 && other_hight > hight && !nodes.include?(next_node) && !next_nodes.include?(next_node)
        end
        if current_node[1] < 99
            next_node = [current_node[0], current_node[1] + 1]
            other_hight = hightmap[current_node[0]][current_node[1] + 1]
            next_nodes << next_node if other_hight < 9 && other_hight > hight && !nodes.include?(next_node) && !next_nodes.include?(next_node)
        end
    end
    basin_sizes << nodes.size
end

pp basin_sizes.sort.pop(3).reduce(&:*)