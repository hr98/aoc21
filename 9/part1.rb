hightmap = [];
total_risk_level = 0;

File.open('input.txt').each do |line|
    line = line.split('')
    line.pop(2)
    line.map!(&:to_i)
    hightmap << line
end

hightmap.each_index do |row_i|
    hightmap[row_i].each_index do |column_i|
        hight = hightmap[row_i][column_i]
        next if row_i > 0 && hight >= hightmap[row_i - 1][column_i]
        next if row_i < hightmap.size - 1 && hight >= hightmap[row_i + 1][column_i]
        next if column_i > 0 && hight >= hightmap[row_i][column_i - 1]
        next if column_i < hightmap[row_i].size - 1 && hight >= hightmap[row_i][column_i + 1]
        total_risk_level += hight + 1
    end
end

pp total_risk_level